'use strict';

module.exports = AuthPages;

var AUTH_PAGES = {
  signin: 'signin',
  signup: 'signup',
  forgot: 'forgot',
  restore: 'restore'
};

function AuthPages($config) {
  this._config = $config;
}

AuthPages.prototype._config = null;
AuthPages.prototype.$lifetime = 3600;

AuthPages.prototype.load = function () {
  var currentPage = this.$context.state.page || 'login';

  if (currentPage == 'auth')
    currentPage = 'login';

  if (!AUTH_PAGES.hasOwnProperty(currentPage)) { //TODO: fix favicon loading
    return {
      title: 'Page not found',
      subtitle: '404',
      currentPage: 'err404',
      isActive: {err404: true}
    };
  }

  var result = {
    title: this._config.title,
    subtitle: AUTH_PAGES[currentPage],
    currentPage: currentPage,
    isActive: {}
  };

  Object.keys(AUTH_PAGES)
    .forEach(function (page) {
      result.isActive[page] = (currentPage === page);
    });

  return result;
};