'use strict';

module.exports = err404;

function err404($config) {
  this.config = $config;
}

err404.prototype.load = function () {
    return {};
};