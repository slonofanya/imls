'use strict';

module.exports = Pages;

var PAGES = {
  index: 'index',
  auth: 'auth'
};

var token;

function Pages($config) {
  this._config = $config;
}

Pages.prototype._config = null;

Pages.prototype.$lifetime = 3600;

Pages.prototype.load = function () {
  var currentPage = this.$context.state.page || 'index';


  //console.log(this.$context.state);

  if (!PAGES.hasOwnProperty(currentPage)) { //TODO: fix favicon loading
    //throw new Error(currentPage + ' page not found');
    return  {
      title: 'Page not found',
      subtitle: '404',
      currentPage: 'err404',
      isActive: {err404:true}
    };
  }


  var result = {
    title: this._config.title,
    subtitle: PAGES[currentPage],
    currentPage: currentPage,
    isActive: {}
  };

  Object.keys(PAGES).forEach(function (page) {
    result.isActive[page] = (currentPage === page);
  });

  return result;
};

