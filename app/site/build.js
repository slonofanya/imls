'use strict';

var isRelease = process.argv.length === 3 ?
  process.argv[2] === 'release' : undefined,
  templateEngine = require('catberry-handlebars'),
  catberry = require('catberry'),
  cat = catberry.create({
    isRelease: isRelease,
    publicDirectoryPath: 'public/js'
  })
  ;

templateEngine.register(cat.locator);
cat.build();

