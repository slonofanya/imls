'use strict';

var _ = require('underscore');

module.exports = header;

/*
 * This is a Catberry Cat-component file.
 * More details can be found here
 * https://github.com/catberry/catberry/blob/master/docs/index.md#cat-components
 */

/**
 * Creates new instance of "login" component.
 * @constructor
 */
function header($config, $uhr) {
  this.config = $config;
  this._uhr = $uhr;
}

/**
 * Gets data for template.
 * @returns {Promise<Object>} Promise for data.
 */
_.extend(header.prototype, {
  render: function () {
    return this.$context.getStoreData();
  },

  bind: function () {
    return {
      'click': {
        '.nav-bar-btn-login': function (e) {
          e.preventDefault();
          this.$context.redirect('/auth/signin');
        },
        '.nav-bar-btn-contact': function () {
          this.$context.redirect('/contacts');
        },
        '.nav-bar-btn-search': function () {
          $("#search-popup").toggle();
        },
        '.search-btn-cancel' :function(){
          $("#search-popup").hide();
        }
      }
    }
  }
})
;