'use strict';

module.exports = Navigation;

/*
 * This is a Catberry Cat-component file.
 * More details can be found here
 * https://github.com/catberry/catberry/blob/master/docs/index.md#cat-components
 */

function Navigation() {

}

Navigation.prototype.render = function () {
	return this.$context.getStoreData();
};