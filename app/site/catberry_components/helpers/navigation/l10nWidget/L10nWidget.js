'use strict';

module.exports = L10nWidget;

function L10nWidget($localizationProvider) {
	this._l10n = $localizationProvider;
}

L10nWidget.prototype.bind = function () {
	var self = this;
	$(this.$context.element.children[0]).dropdown({
		action: 'select',
		onChange: function (value, text, $selectedItem) {
			document.getElementById('locale-icon').className =
				$selectedItem[0].children[0].className;
			self._l10n.changeLocale(value, self.$context);
		}
	});
};

L10nWidget.prototype.unbind = function () {};

L10nWidget.prototype.render = function () {
	var className = this._l10n.getCurrentLocale(this.$context),
		CLASSES = {
			'en-us': 'united states flag',
			ru: 'russia flag',
			uk: 'ukraine flag'
		};

	return {
		currentLocaleClass: CLASSES[className]
	};
};