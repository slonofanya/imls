'use strict';

module.exports = header;

/*
 * This is a Catberry Cat-component file.
 * More details can be found here
 * https://github.com/catberry/catberry/blob/master/docs/index.md#cat-components
 */

/**
 * Creates new instance of "head" component.
 * @param {Object} $config Catberry application config.
 * @constructor
 */
function header($config) {
	this._config = $config;
}

/**
 * Current config.
 * @type {Object}
 * @private
 */
header.prototype._config = null;

/**
 * Gets data for template.
 * @returns {Object} Data object.
 */
header.prototype.render = function () {
	return this._config;
};
