'use strict';

var _ = require('underscore');

module.exports = Restore;

/*
 * This is a Catberry Cat-component file.
 * More details can be found here
 * https://github.com/catberry/catberry/blob/master/docs/index.md#cat-components
 */

/**
 * Creates new instance of "login" component.
 * @constructor
 */
function Restore($config, $uhr) {
    this.config = $config;
    this._uhr = $uhr;
}

/**
 * Gets data for template.
 * @returns {Promise<Object>} Promise for data.
 */
_.extend(Restore.prototype, {
    render: function () {
        return this.$context.getStoreData();
    },

    bind: function () {
        return {
            'click': {
                '.js-btn-Restore': function (e) {
                    e.preventDefault();
                    var options = {
                        timeout: 3000,
                        data: {
                            password: this.$context.element.querySelector("input[name=password]").value,
                            passwordConfirm: this.$context.element.querySelector("input[name=password_confirm]").value
                        }
                    };

                    if(options.data.password!=options.data.passwordConfirm){
                        $(this.$context.element.querySelector("input[name=password_confirm]").parentElement).addClass("has-error") ;
                        console.log(this.$context.element.querySelector("input[name=password_confirm]"));

                    } else{

                    this._uhr.post(this.config.url.Restore, options)
                        .then(function (result) {
                            if (result.status.code != 200) {
                                alert(result.content);
                            }
                            else {
                                console.log(this);
                                //this.$context.cookie.set('Authorization uid: ' + session.sid);
                                //this.$context.redirect(this.config.url.me);
                            }
                        });
                    }

                },
                'li.js-btn-login': function () {
                    this.$context.redirect('login');
                },
                'li.js-btn-register': function () {
                    this.$context.redirect('register');
                }
            }
        }
    }
})
;