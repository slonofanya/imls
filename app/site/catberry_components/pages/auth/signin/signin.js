'use strict';

var _ = require('underscore');

module.exports = Signin;

/*
 * This is a Catberry Cat-component file.
 * More details can be found here
 * https://github.com/catberry/catberry/blob/master/docs/index.md#cat-components
 */

/**
 * Creates new instance of "login" component.
 * @constructor
 */
function Signin($config, $uhr) {
  this.config = $config;
  this._uhr = $uhr;
}

Signin.prototype._uhr = null;

/**
 * Gets data for template.
 * @returns {Promise<Object>} Promise for data.
 */
_.extend(Signin.prototype, {
  render: function () {
    return this.$context.getStoreData();
  },


  bind: function () {
    return {
      click: {
        '.js-close': function (e) {
          $("#myModal").hide();
        }
      },
      submit: {
        '.js-form-signin': function (e) {
          e.preventDefault();

          var options = {
              timeout: 3000,
              data: {
                email: this.$context.element.querySelector("input[name=email]").value,
                password: this.$context.element.querySelector("input[name=password]").value
              }
            },
            this_component = this,
            $context = this_component.$context;

          this._uhr.post(this.config.url.signin, options)
            .then(function (result) {
              if (result.status.code == 200) {
                $context.cookie.set(
                  {
                    key: 'Authorization',
                    value: 'JWT ' + result.content.token,
                    path: '/'
                  });
                $context.redirect("/profile");
                return;
              }

              $("#myModal").show();

              //alert(result.content.code);
            });
        }
      }
    }
  }
})
;