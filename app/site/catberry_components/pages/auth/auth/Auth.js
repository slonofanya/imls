'use strict';

var _ = require('underscore');

module.exports = Auth;

/*
 * This is a Catberry Cat-component file.
 * More details can be found here
 * https://github.com/catberry/catberry/blob/master/docs/auth.md#cat-components
 */

/**
 * Creates new instance of "auth" component.
 * @constructor
 */
function Auth() {
}

/**
 * Gets data for template.
 * @returns {Promise<Object>} Promise for data.
 */
_.extend(Auth.prototype, {
  render: function () {
    return this.$context.getStoreData('Pages');
  }
});