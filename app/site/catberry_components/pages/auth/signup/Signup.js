'use strict';

var _ = require('underscore');

module.exports = Signup;

/*
 * This is a Catberry Cat-component file.
 * More details can be found here
 * https://github.com/catberry/catberry/blob/master/docs/index.md#cat-components
 */

/**
 * Creates new instance of "login" component.
 * @constructor
 */
function Signup($config, $uhr) {
  this.config = $config;
  this._uhr = $uhr;
}

/**
 * Gets data for template.
 * @returns {Promise<Object>} Promise for data.
 */
_.extend(Signup.prototype, {
  render: function () {
    return this.$context.getStoreData();
  },

  bind: function () {
    return {
      'click': {
        '.js-btn-signup': function (e) {
          e.preventDefault();
          //if (this.$context.element.querySelector("input[name=password]").value.match(/^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\)) {
            var options = {
                timeout: 3000,
                data: {
                  firstName: this.$context.element.querySelector("input[name=first_name]").value,
                  lastName: this.$context.element.querySelector("input[name=last_name]").value,
                  email: this.$context.element.querySelector("input[name=email]").value,
                  web: this.$context.element.querySelector("input[name=web]").value,
                  officePhone: this.$context.element.querySelector("input[name=office_phone]").value,
                  cellPhone: this.$context.element.querySelector("input[name=office_phone]").value,
                  homePhone: this.$context.element.querySelector("input[name=home_phone]").value,
                  address: this.$context.element.querySelector("input[name=address]").value,
                  city: this.$context.element.querySelector("input[name=city]").value,
                  state: this.$context.element.querySelector("select[name=state]").value,
                  postalCode: this.$context.element.querySelector("input[name=postal_code]").value,
                  country: this.$context.element.querySelector("select[name=country]").value,
                  comments: this.$context.element.querySelector("select[name=country]").value
                }
              },
              this_component = this,
              $context = this_component.$context;
            this._uhr.post(this.config.url.signup, options)
              .then(function (result) {
                if (result.status.code != 200) {
                }
                else {
                  $context.redirect(this_component.config.url.me);
                }
              });

          //}
        }
      }
    }
  }
})
;