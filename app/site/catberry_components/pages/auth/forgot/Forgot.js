'use strict';

var _ = require('underscore');

module.exports = Forgot;

/*
 * This is a Catberry Cat-component file.
 * More details can be found here
 * https://github.com/catberry/catberry/blob/master/docs/index.md#cat-components
 */

/**
 * Creates new instance of "login" component.
 * @constructor
 */
function Forgot($config, $uhr) {
    this.config = $config;
    this._uhr = $uhr;
}

/**
 * Gets data for template.
 * @returns {Promise<Object>} Promise for data.
 */
_.extend(Forgot.prototype, {
    render: function () {
        return this.$context.getStoreData();
    },

    bind: function () {
        return {
            'click': {
                'a.js-btn-forgot': function (e) {
                    e.preventDefault();
                    var options = {
                        timeout: 3000,
                        data: {
                            username: this.$context.element.querySelector("input[name=email]").value,
                            password: this.$context.element.querySelector("input[name=password]").value
                        }
                    };
                    this._uhr.post(this.config.url.forgot, options)
                        .then(function (result) {
                            if (result.status.code != 200) {
                                alert(result.content);
                            }
                            else {
                                console.log(this);
                                //this.$context.cookie.set('Authorization uid: ' + session.sid);
                                //this.$context.redirect(this.config.url.me);
                            }
                        });

                },
                'li.js-btn-login': function () {
                    this.$context.redirect('login');
                },
                'li.js-btn-register': function () {
                    this.$context.redirect('register');
                }
            }
        }
    }
})
;