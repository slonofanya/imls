'use strict';

var _ = require('underscore');

module.exports = Index;

/*
 * This is a Catberry Cat-component file.
 * More details can be found here
 * https://github.com/catberry/catberry/blob/master/docs/index.md#cat-components
 */

/**
 * Creates new instance of "index" component.
 * @constructor
 */
function Index($config, $uhr) {
    this.config = $config;
    this._uhr = $uhr;
}

/**
 * Gets data for template.
 * @returns {Promise<Object>} Promise for data.
 */
_.extend(Index.prototype, {
    render: function () {
        return this.$context.getStoreData();
    },

    bind: function () {
        return {

        }
    }
})
;