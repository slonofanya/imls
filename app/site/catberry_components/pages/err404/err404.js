'use strict';
module.exports = err404;

/*
 * This is a Catberry Cat-component file.
 * More details can be found here
 * https://github.com/catberry/catberry/blob/master/docs/index.md#cat-components
 */

/**
 * Creates new instance of "login" component.
 * @constructor
 */
function err404($config, $uhr) {
    this.config = $config;
}

/**
 * Gets data for template.
 * @returns {Promise<Object>} Promise for data.
 */
err404.prototype.render = function () {
  return this.$context.getStoreData();
};
//
err404.prototype.bind = function () {
  return { };
};

