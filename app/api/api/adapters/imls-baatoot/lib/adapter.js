/**
 * Created by Luigi Ilie Aron on 27.01.15.
 * email: luigi@kreditech.com
 */

var Errors = require('waterline-errors').adapter,
    Connection = require('./connection'),
    Collection = require('./collection'),
    util = require('util'),
    _ = require('lodash'),
    Promise = require("bluebird")
//Lookup = require('Lookup')
    ;

const typeSeparator = '::';
const ERROR_NON_UNIQUE = 'uniqError';

module.exports = (function () {

    // Keep track of all the connections used by the app
    var connections = {};
    var db = null;

    var adapter = {
            // Which type of primary key is used by default
            pkFormat: 'string',
            // to track schema internally
            syncable: true,
            //identity: "imls-baatoot",
            schema: false,
            // Default configuration for connections
            defaults: {
                // couch base
                cb: {
                    //Server
                    host: 'localhost',
                    port: 8091,
                    user: 'admin',
                    pass: 'password',

                    //bucket
                    bucket: {
                        name: 'testCbes',
                        pass: ''
                    }
                },
                // elastic search
                es: {
                    host: ['127.0.0.1:9200'],
                    log: 'error',
                    index: 'test_cbes',
                    numberOfShards: 5,
                    numberOfReplicas: 1
                }
            },

            /**
             * @param connection
             * @param collections
             * @param cb
             * @returns {*}
             */
            registerConnection: function (connection, collections, cb) {

                if (!connection.identity) return cb(Errors.IdentityMissing);
                if (connections[connection.identity]) return cb(Errors.IdentityDuplicate);

                // Store the connection
                connections[connection.identity] = {
                    config: connection,
                    collections: {}
                };

                // Create new connection
                new Connection(connection, collections, function (_err, connectionObject) {
                    if (_err) {
                        return cb((function _createError() {
                            var msg = util.format('Failed to connect to the Couchbase/ElasticSearch clients', util.inspect(_err, false, null));
                            var err = new Error(msg);
                            err.originalError = _err;
                            return err;
                        })());
                    }

                    connections[connection.identity].connection = connectionObject;
                    Object.keys(collections).forEach(function (key) {
                        connections[connection.identity].collections[key] = new Collection(collections[key], key, connectionObject);
                    });

                    cb();
                });
            },

            /**
             * Fired when a model is unregistered, typically when the server
             * is killed. Useful for tearing-down remaining open connections,
             * etc.
             *
             * @param  {Function} cb [description]
             * @return {[type]}      [description]
             */
            teardown: function (conn, cb) {
                if (typeof conn == 'function') {
                    cb = conn;
                    conn = null;
                }

                if (!conn) {
                    connections = {};
                    return cb();
                }

                if (!connections[conn]) return cb();
                delete connections[conn];

                cb();
            },

            describe: function (connection, collection, cb) {
                // Add in logic here to describe a collection (e.g. DESCRIBE TABLE logic)
                return cb();
            },

            /**
             *
             * REQUIRED method if integrating with a schemaful
             * (SQL-ish) database.
             *
             */
            define: function (connection, collection, definition, cb) {
                // Add in logic here to create a collection (e.g. CREATE TABLE logic)
                return cb();
            },

            /**
             *
             * REQUIRED method if integrating with a schemaful
             * (SQL-ish) database.
             *
             */
            drop: function (connection, collection, relations, cb) {
                // Add in logic here to delete a collection (e.g. DROP TABLE logic)
                return cb();
            },


            getModel: function (collection) {
                var model;
                for (var modelIndex in this.sails.models) {
                    if (this.sails.models[modelIndex].adapter.collection == collection) {
                        model = sails.models[modelIndex];
                        break;
                    }
                }
                return model;
            },

            getUniqueFields: function (attributes) {
                var uniqueAttributes = [];
                for (var attrIndex in attributes) {
                    if (attributes[attrIndex].unique && !attributes[attrIndex].primaryKey) {
                        uniqueAttributes.push(attrIndex);
                    }
                }

                return uniqueAttributes;
            },


            /**
             * @param connection
             * @param collection
             * @param options
             * @param callback
             */
            find: function (connection, collection, options, callback) {

                var model = this.getModel(collection);
                var uniqueAttrs = this.getUniqueFields(model.attributes);
                var db = require('./db')(connections[connection].connection);
                db.find(collection, options, function (err, res) {
                    if (err) {
                        return callback(err, false);
                    }


                    callback(null, res);
                });
            },

            findOne: function (connection, collection, options, callback) {
                self = this;
                var db = require('./db')(connections[connection].connection);

                if (typeof(options.where.id) != 'undefined') {
                    db.get(collection, options.where.id, function (err, res) {
                        if (err) {
                            return callback(err, false);
                        }
                        var resultSet = null;
                        if (res && res.value)
                            resultSet = [res.value];

                        return callback(null, resultSet);
                    });

                    return;
                }


                var model = this.getModel(collection);
                var uniqueAttrs = this.getUniqueFields(model.attributes);

                var haveUniqueKey = false;

                for (var key in options.where) {
                    for (var uniqueKey in uniqueAttrs) {
                        if (uniqueAttrs == key) {
                            haveUniqueKey = key;
                            break;
                        }
                    }
                    if (haveUniqueKey !== false) break;
                }

                if (haveUniqueKey !== false) {
                    var Lookup = this.getModel('lu_');
                    db.get(Lookup.tableName, Lookup.hashId(collection, key, options.where[key]), function (err, res) {
                        if (err) {
                            return callback(err, false);
                        }
                        var resultSet = null;

                        if (res && res.value && typeof(res.value.lookup_id) != 'undefined') {

                            options.where.id = res.value.lookup_id;

                            self.findOne(connection, collection, options, callback);
                        } else {
                            return callback(null, null);
                        }

                    });
                } else {
                    return this.find(connection, collection, options, callback);
                }
            },

            /**
             * @param connection
             * @param collection
             * @param values
             * @param callback
             */
            create: function (connection, collection, values, callback) {

                var db = require('./db')(connections[connection].connection),
                    originalVals = _.cloneDeep(values),
                    _values = [];

                var model = this.getModel(collection);
                var uniqueAttrs = this.getUniqueFields(model.attributes);
                var Lookup = this.getModel('lu_');
                var blockingLookUpPromises = [];
                for (var index in uniqueAttrs) {
                    blockingLookUpPromises.push(
                        new Promise(function (resolve, reject) {
                            var LookupId = Lookup.hashId(collection, uniqueAttrs[index], values[uniqueAttrs[index]]);
                            Lookup.findOne({id: LookupId}).exec(function (err, res) {
                                if (res && res.doc_id) {
                                    reject(uniqueAttrs[index]);
                                }
                                resolve(uniqueAttrs[index]);
                            });

                        })
                    );
                }

                Promise.all(blockingLookUpPromises).then(function () {
                    db.create(collection, values, function (err, _vals) {
                        if (err) {
                            return callback(err, false);
                        }
                        for (var index in uniqueAttrs) {
                            var LookupId = Lookup.hashId(collection, uniqueAttrs[index], _vals[uniqueAttrs[index]]);
                            Lookup.create(
                                {
                                    doc_id: LookupId,
                                    lookup_collection: collection,
                                    lookup_id: _vals.id,
                                    key: uniqueAttrs[index],
                                    value: _vals[uniqueAttrs[index]]
                                }
                            ).exec(function (err, res) {
                                    //todo some debug
                                })
                        }

                        return callback(null, _vals);

                    });
                }).catch(function (_err) {
                        return callback(
                            {
                                code: "notUnique",
                                message: "non unique Value",
                                field: _err
                            })
                    }
                )
            }

            ,

            /**
             * @param connection
             * @param collection
             * @param options
             * @param values
             * @param callback
             */
            update: function (connection, collection, options, values, callback) {
                var db = require('./db')(connections[connection].connection);

                db.update(collection, options, values, function (err, res) {
                    if (err) {
                        return callback(err, false);
                    }

                    callback(null, res);
                });
            }
            ,

            /**
             * @param connection
             * @param collection
             * @param options
             * @param callback
             */
            destroy: function (connection, collection, options, callback) {
                var connectionObject = connections[connection].connection,
                    bucket = connectionObject.db.cb,
                    esClient = connectionObject.db.es;

                if (
                    options &&
                    typeof options == 'object' &&
                    Object.keys(options).length == 1 &&
                    options.hasOwnProperty('where') &&
                    options.where instanceof Object &&
                    options.where !== null &&
                    Object.keys(options.where).length == 1 &&
                    options.where.hasOwnProperty('id')
                ) {
                    options.where = {
                        bool: {
                            must: [
                                {
                                    term: {
                                        _id: options.where.id
                                    }
                                }
                            ]
                        }
                    };
                }

                var db = require('./db')(connections[connection].connection);
                db.destroy(collection, options, function (err, res) {
                    if (err) {
                        return callback(err, false);
                    }

                    callback(null, res);
                });
            }
            ,

            /**
             * @param connection
             * @param collection
             * @param options
             * @param callback
             */
            count: function (connection, collection, options, callback) {
                var db = require('./db')(connections[connection].connection);
                db.count(collection, options, function (err, res) {
                    if (err) {
                        return callback(err, false);
                    }

                    callback(null, res);
                });
            }
            ,

            /**
             * get raw data from couchbase view
             * @param connection
             * @param collection
             * @param callback
             */
            getRawCollection: function (connection, collection, callback) {
                var db = require('./db')(connections[connection].connection);

                db.getCollection(collection, function (err, res) {
                    if (err) {
                        return callback(err, false);
                    }

                    callback(null, res);
                });
            }
        }
        ;

    return adapter;
})
();