var passport = require('passport');
var Promise = require("bluebird");
/**
 * Triggers when user authenticates via passport
 * @param {Object} req Request object
 * @param {Object} res Response object
 * @param {Object} error Error object
 * @param {Object} user User profile
 * @param {Object} info Info if some error occurs
 * @private
 */
function _onPassportAuth(req, res, error, user, info) {
    if (error) return res.serverError(error);
    if (!user) return res.unauthorized(null, info && info.code, info && info.message);

    return res.ok({
        // TODO: replace with new type of cipher service
        token: CipherService.createToken(user),
        user: user
    });
}

module.exports = {
    /**
     * Sign up in system
     * @param {Object} req Request object
     * @param {Object} res Response object
     */
    signup: function (req, res) {
        //User
        //    .create(_.omit(req.allParams(), 'id'))
        //    .then(function (user) {
        //        console.log('create.then', arguments);
        //
        //        return {
        //            // TODO: replace with new type of cipher service
        //            token: CipherService.createToken(user),
        //            user: user
        //        };
        //    })
        //    .then(
        //    function () {
        //        console.log('create.then.then', arguments);
        //        res.created;
        //    }
        //).fail( function () {console.log('fail',arguments)}
        //    .catch(
        //    function () {
        //       // console.log('create.then.then.catch', arguments);
        //        res.serverError
        //    });
        //

        create = new Promise(function (success, failure) {

                User.create(_.omit(req.allParams(), 'id')).exec(function (err, result) {
                    if (err) return failure(err);
                    success(result);
                })
            }
        );


        create.then(function () {
                console.log('then', arguments)
            }
        ).catch(
            function (_err) {

                err=_err.originalError;
                console.log('4to', err.code);
                console.log('kuda', err.message);
                console.log('pachimu', err.field);


            });


        //

    },

    /**
     * Sign in by local strategy in passport
     * @param {Object} req Request object
     * @param {Object} res Response object
     */
    signin: function (req, res) {
        passport.authenticate('local',
            _onPassportAuth.bind(this, req, res))(req, res);
    },
};