module.exports = {
  tableName: 'u_',


  attributes: {
    firstName: {
      type:'string',
      notEmpty:true
    },
    lastName: {
      type:'string',
      notEmpty:true
    },
    status: {type: 'boolean', defaultsTo: false},
    email: {
      type: 'string',
      required: true,
      unique: true
    },
    web: {
      type:'url'
    },
    officePhone: {
      type:'numeric'
    },
    cellPhone: {
      type:'numeric'
    },
    homePhone: {
      type:'numeric'
    },
    address: {
      type:'string',
      notEmpty:true
    },
    city:{
      type:'string',
      notEmpty:true
    },
    state: {
      type:'string',
      notEmpty:true
    },
    postalCode: {
      type:'integer',
      notEmpty:true
    },
    country: {
      type:'string',
      notEmpty:true
    },
    password: {
      type: 'string',
      required: false
    },
    // override default toJSON
    toJSON: function() {
      var obj = this.toObject();
      delete obj.password;
      return obj;
    }
  },

  beforeUpdate: function (values, next) {
    CipherService.hashPassword(values);
    next();
  },
  beforeCreate: function (values, next) {
    CipherService.hashPassword(values);
    next();
  },


  mapping: {
    "_all": {
      "enabled": false
    },
    firstName: {
      type: 'string',
      analyzer: 'whitespace',
      fields: {
        raw: {
          type: 'string',
          index: 'not_analyzed'
        }
      }
    },
    lastName: {
      type: 'string',
      analyzer: 'whitespace'
    },
    email: {
      type: 'string',
      analyzer: 'standard'
    },
    phone: {
      type: 'string',
      analyzer: 'keyword'
    },
    createdAt: {
      type: 'date',
      format: 'dateOptionalTime'
    },
    updatedAt: {
      type: 'date',
      format: 'dateOptionalTime'
    },
    status: {
      type: 'boolean'
    }
  }
};