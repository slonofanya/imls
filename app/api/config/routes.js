
module.exports.routes = {

  '/': {  view: 'homepage' },
  'post /user/signup': 'AuthController.signup',
  'post /user/signin': 'AuthController.signin'

};
