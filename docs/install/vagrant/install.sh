#!/bin/sh

cp /vagrant/iMLS_api.conf /etc/nginx/sites-available/
cp /vagrant/iMLS_site.conf /etc/nginx/sites-available/

ln -s /etc/nginx/sites-available/iMLS_api.conf /etc/nginx/sites-enabled/
ln -s /etc/nginx/sites-available/iMLS_site.conf /etc/nginx/sites-enabled/

sudo dpkg -i couchbase-server-enterprise_3.0.3-ubuntu12.04_amd64.deb

service nginx restart